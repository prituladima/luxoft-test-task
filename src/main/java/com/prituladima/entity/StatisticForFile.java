package com.prituladima.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static com.prituladima.SQLHelper.*;
import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by prituladima on 25.10.16.
 */
@Entity
@Table(name = TABLE_FILES_NAME, catalog = DATABASE_NAME)
public class StatisticForFile implements java.io.Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ID_FIELD_FILES, unique = true, nullable = false)
    private Integer id;

    @Column(name = NAME_FIELD_FILES)
    private String name;

    @Column(name = LONGEST_WORD_FIELD_FILES)
    private String longestWord;

    @Column(name = SHORTEST_WORD_FIELD_FILES)
    private String shortestWord;

    @Column(name = NUMBER_OF_LINES_FIELD_FILES)
    private Integer numberOfLines;

    @Column(name = AVERAGE_FIELD_FILES)
    private Integer average;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "statisticForFile")
    private List<Statistic> statisticList = new ArrayList<Statistic>(0);


    public Integer getId() {
        return id;
    }

    public StatisticForFile setId(Integer id) {
        this.id = id;
        return this;
    }



    public String getName() {
        return name;
    }

    public StatisticForFile setName(String name) {
        this.name = name;
        return this;
    }


    public String getLongestWord() {
        return longestWord;
    }

    public StatisticForFile setLongestWord(String longestWord) {
        this.longestWord = longestWord;
        return this;
    }


    public String getShortestWord() {
        return shortestWord;
    }

    public StatisticForFile setShortestWord(String shortestWord) {
        this.shortestWord = shortestWord;
        return this;
    }


    public Integer getNumberOfLines() {
        return numberOfLines;
    }

    public StatisticForFile setNumberOfLines(Integer numberOfLines) {
        this.numberOfLines = numberOfLines;
        return this;
    }


    public Integer getAverage() {
        return average;
    }

    public StatisticForFile setAverage(Integer average) {
        this.average = average;
        return this;
    }


    public List<Statistic> getStatisticList() {
        return statisticList;
    }

    public StatisticForFile setStatisticList(List<Statistic> statisticList) {
        this.statisticList = statisticList;
        return this;
    }

    @Override
    public String toString() {
        return "StatisticForFile{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", longestWord='" + longestWord + '\'' +
                ", shortestWord='" + shortestWord + '\'' +
                ", numberOfLines=" + numberOfLines +
                ", average=" + average +
                ", statisticList=" + statisticList +
                '}';
    }

}
