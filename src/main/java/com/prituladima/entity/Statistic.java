package com.prituladima.entity;

import org.hibernate.annotations.Type;

import javax.persistence.*;

import static com.prituladima.SQLHelper.*;

/**
 * Created by prituladima on 10/22/16.
 */
@Entity
@Table(name = TABLE_LINES_NAME, catalog = DATABASE_NAME)
public class Statistic implements java.io.Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ID_FIELD_LINES, unique = true, nullable = false)
    private Integer id;

    @Type(type = "text")
    @Column(name = LINE_FIELD_LINES, length=4048)
    private String line;

    @Column(name = LONGEST_WORD_FIELD_LINES)
    private String longestWord;

    @Column(name = SHORTEST_WORD_FIELD_LINES)
    private String shortestWord;

    @Column(name = LENGTH_FIELD_LINES)
    private Integer length;

    @Column(name = AVERAGE_FIELD_LINES)
    private Integer average;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = ID_FIELD_FILES, nullable = false)
    private StatisticForFile statisticForFile;


    public Integer getId() {
        return id;
    }

    public Statistic setId(int id) {
        this.id = id;
        return this;
    }


    public String getLine() {
        return line;
    }

    public Statistic setLine(String line) {
        this.line = line;
        return this;
    }


    public String getLongestWord() {
        return longestWord;
    }

    public Statistic setLongestWord(String longestWord) {
        this.longestWord = longestWord;
        return this;
    }


    public String getShortestWord() {
        return shortestWord;
    }

    public Statistic setShortestWord(String shortestWord) {
        this.shortestWord = shortestWord;
        return this;
    }


    public Integer getLength() {
        return length;
    }

    public Statistic setLength(int length) {
        this.length = length;
        return this;
    }


    public Integer getAverage() {
        return average;
    }

    public Statistic setAverage(int average) {
        this.average = average;
        return this;
    }

    public StatisticForFile getStatisticForFile() {
        return statisticForFile;
    }

    public Statistic setStatisticForFile(StatisticForFile statisticForFile) {
        this.statisticForFile = statisticForFile;
        return this;
    }

    @Override
    public String toString() {
        return "com.prituladima.entity.Statistic{" +
                "line='" + line + '\'' +
                ", longestWord='" + longestWord + '\'' +
                ", shortestWord='" + shortestWord + '\'' +
                ", length=" + length +
                ", average=" + average +
                '}';
    }
}
