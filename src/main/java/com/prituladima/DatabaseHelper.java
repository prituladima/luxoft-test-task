package com.prituladima;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by prituladima on 10/23/16.
 */
public class DatabaseHelper {

    private static final String CLASS_NAME = "com.mysql.jdbc.Driver";
    private static final String PATH = "jdbc:mysql://127.0.0.1:3306/";
    private static final String DATABASE_NAME = SQLHelper.DATABASE_NAME;
    private static final String USER = "root";
    private static final String PASSWORD = "adminadmin";

    static Connection initDataBase() {
        System.out.println("\n-------- MySQL JDBC Connection Testing ------------");

        try {

            Class.forName(CLASS_NAME);

        } catch (ClassNotFoundException e) {

            System.out.println("Where is your MySQL JDBC Driver? Include in your library path!");
            e.printStackTrace();
            return null;

        }

        System.out.println("MySQL JDBC Driver Registered!");

        Connection connection = null;

        try {
            connection = DriverManager.getConnection(PATH + DATABASE_NAME, USER, PASSWORD);
        } catch (SQLException e) {
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return null;
        }

        if (connection != null) {
            System.out.println("You made it, take control your database now!");
            return connection;
        } else {
            System.out.println("Failed to make connection!");
            return null;
        }
    }
}
