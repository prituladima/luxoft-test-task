package com.prituladima;

import com.prituladima.entity.Statistic;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by prituladima on 10/23/16.
 */
public class FileReader {

    public static final String FILE_NAME = "test.txt";

    List<String> readFromFile() {
        return readFromFile(new File(FILE_NAME));

    }

    List<String> readFromFile(File file) {
        List<String> linesFromFile = new ArrayList<String>();
        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String line;
                if (!(line = scanner.nextLine()).equals(""))
                    linesFromFile.add(line);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return linesFromFile;

    }

}
