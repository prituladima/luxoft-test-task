package com.prituladima;

import com.prituladima.entity.Statistic;
import com.prituladima.entity.StatisticForFile;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.prituladima.SQLHelper.*;

public class ConsoleApplicationJDBC {

    public static void main(String[] args) {
        new ConsoleApplicationJDBC().startWork();
    }

    private void startWork() {

        List<File> fileList = new ArrayList<File>();
        new FolderHelper().listFilesForFolder(new File("book"), fileList);

        for(File current: fileList) {
            List<String> linesFromFile = new FileReader().readFromFile(current);
            StatisticForFile statisticForFile = Mapper.mapToStatisticForFile(linesFromFile, current);
            Connection connection = DatabaseHelper.initDataBase();
            writeToDataBase(connection, statisticForFile);
        }

    }


    private void writeToDataBase(Connection dbConnection, StatisticForFile statisticForFile) {

        System.out.println(statisticForFile);

        PreparedStatement preparedStatementForFile = null;
        String insertFileInfoSQL = COMMAND_INSERT +
                TABLE_FILES_NAME +
                "(" +
                NAME_FIELD_FILES + ", " +
                LONGEST_WORD_FIELD_FILES +  ", " +
                SHORTEST_WORD_FIELD_FILES + ", " +
                NUMBER_OF_LINES_FIELD_FILES + ", " +
                AVERAGE_FIELD_FILES +
                ")" +
                PREPARED_VALUES
                + "(?,?,?,?,?)";

        PreparedStatement preparedStatementForLines = null;
        String insertLinesSQL =
                COMMAND_INSERT +
                        TABLE_LINES_NAME +
                        "(" +
                        LINE_FIELD_LINES + ", " +
                        LONGEST_WORD_FIELD_LINES + ", " +
                        SHORTEST_WORD_FIELD_LINES + ", " +
                        LENGTH_FIELD_LINES + ", " +
                        AVERAGE_FIELD_LINES + ", " +
                        FILE_ID_FIELD_LINES +
                        ")" +
                        PREPARED_VALUES
                        + "(?,?,?,?,?,?)";

        try {
            dbConnection.setAutoCommit(false);
            preparedStatementForFile = dbConnection.prepareStatement(insertFileInfoSQL, Statement.RETURN_GENERATED_KEYS);

            preparedStatementForFile.setString(1, statisticForFile.getName());
            preparedStatementForFile.setString(2, statisticForFile.getLongestWord());
            preparedStatementForFile.setString(3, statisticForFile.getShortestWord());
            preparedStatementForFile.setInt(4, statisticForFile.getNumberOfLines());
            preparedStatementForFile.setInt(5, statisticForFile.getAverage());
            preparedStatementForFile.executeUpdate();

            ResultSet rs = preparedStatementForFile.getGeneratedKeys();
            rs.next();
            int justCreatedFileID = rs.getInt(1);


            preparedStatementForLines = dbConnection.prepareStatement(insertLinesSQL);

            for (Statistic current : statisticForFile.getStatisticList()) {
                preparedStatementForLines.setString(1, current.getLine());
                preparedStatementForLines.setString(2, current.getLongestWord());
                preparedStatementForLines.setString(3, current.getShortestWord());
                preparedStatementForLines.setInt(4, current.getLength());
                preparedStatementForLines.setInt(5, current.getAverage());
                preparedStatementForLines.setInt(6, justCreatedFileID);
                preparedStatementForLines.executeUpdate();

                System.out.println("Record is inserted into table!");
                System.out.println(current);

            }
            dbConnection.commit();

        } catch (SQLException e) {

            System.err.println(e.getMessage());

        } finally {
            try {
                if (preparedStatementForLines != null) {
                    preparedStatementForLines.close();
                }

                if (preparedStatementForFile != null) {
                    preparedStatementForFile.close();
                }

                if (dbConnection != null) {
                    dbConnection.close();
                }
            } catch (SQLException e) {
                //ignore
            }

        }

        System.out.println("Done!");


    }
}
