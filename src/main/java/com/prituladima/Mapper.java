package com.prituladima;

import com.prituladima.entity.Statistic;
import com.prituladima.entity.StatisticForFile;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static java.lang.System.out;

/**
 * Created by prituladima on 10/22/16.
 */
public class Mapper {

    private static final String REGEX = " ";

    static StatisticForFile mapToStatisticForFile(List<String> listFromFile, File file) {
        List<Statistic> result = mapToStatisticList(listFromFile, file);
        StatisticForFile statisticForFile = mapToFileStatistic(result, file);
        return statisticForFile;
    }

    static StatisticForFile mapToFileStatistic(List<Statistic> statisticList, File file) {

        String shortestWord = getShortestFromList(statisticList);
        String longestWord = getLongestFromList(statisticList);
        int average = calculateAverage(statisticList);


        return new StatisticForFile()
                .setName(file.getAbsolutePath())
                .setLongestWord(longestWord)
                .setShortestWord(shortestWord)
                .setNumberOfLines(statisticList.size())
                .setAverage(average)
                .setStatisticList(statisticList);
    }


    private static int calculateAverage(List<Statistic> statisticList) {
        if (statisticList.size() == 0)
            return 0;

        int average = 0, sum = 0;
        for (Statistic current : statisticList)
            sum += current.getAverage();

        average = sum / statisticList.size();

        return average;
    }

    private static String getLongestFromList(List<Statistic> statisticList) {
        int maxLength = 0, index = 0, indexMax = 0;
        for (Statistic current : statisticList) {
            if (current.getLongestWord().length() > maxLength) {
                maxLength = current.getLongestWord().length();
                indexMax = index;
            }
            index++;
        }
        return statisticList.get(indexMax).getLongestWord();
    }

    private static String getShortestFromList(List<Statistic> statisticList) {
        int minLength = Integer.MAX_VALUE, index = 0, indexMin = 0;
        for (Statistic current : statisticList) {
            if (current.getShortestWord().length() < minLength) {
                minLength = current.getShortestWord().length();
                indexMin = index;
            }
            index++;
        }
        return statisticList.get(indexMin).getShortestWord();
    }

    private static List<Statistic> mapToStatisticList(List<String> listFromFile, File file) {
        List<Statistic> result = new ArrayList<Statistic>();
        for (String current : listFromFile) {
            result.add(Mapper.map(current));
        }
        return result;
    }

    static Statistic map(String line) {

        String[] wordsInLine = line.split(REGEX);
        int sum = 0, average = 0,
                maxLength = 0, minLength = Integer.MAX_VALUE,
                index = 0, indexMax = 0, indexMin = 0;
        for (String current : wordsInLine) {
            sum += current.length();

            if (current.length() > maxLength) {
                maxLength = current.length();
                indexMax = index;
            }

            if (current.length() < minLength) {
                minLength = current.length();
                indexMin = index;
            }

            index++;
        }
        if (wordsInLine.length != 0)
            average = sum / wordsInLine.length;
        else
            average = 0;
        out.print(average);

        return new Statistic()
                .setLine(line)
                .setLength(line.length())
                .setLongestWord(wordsInLine.length != 0 ? wordsInLine[indexMax]:"")
                .setShortestWord(wordsInLine.length != 0 ? wordsInLine[indexMin]:"")
                .setAverage(average);
    }

    private Mapper() {
    }
}
