package com.prituladima;

import com.prituladima.entity.Statistic;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.prituladima.entity.StatisticForFile;
import com.prituladima.hibernate.SessionFactoryUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * Created by prituladima on 10/23/16.
 */
public class ConsoleApplicationHibernate {

    public static void main(String[] args) {
        new ConsoleApplicationHibernate().startWork();
    }

    private void startWork() {
        List<File> fileList = new ArrayList<File>();
        new FolderHelper().listFilesForFolder(new File("book"), fileList);

        for(File current: fileList) {
            List<String> linesFromFile = new FileReader().readFromFile();
            StatisticForFile statisticList = Mapper.mapToStatisticForFile(linesFromFile, current);
            createStatistic(statisticList);
        }
    }

    private static void createStatistic(StatisticForFile statisticForFile) {
        System.out.println(statisticForFile);

        Transaction transaction = null;
        Session session = SessionFactoryUtil.getInstance().getCurrentSession();
        try {
            transaction = session.beginTransaction();


            session.save(statisticForFile);

            for (Statistic statistic : statisticForFile.getStatisticList()) {
                statistic.setStatisticForFile(statisticForFile);
                session.save(statistic);
            }

            transaction.commit();
        } catch (RuntimeException e) {
            if (transaction != null && transaction.isActive()) {
                try {
// Second try catch as the rollback could fail as well
                    transaction.rollback();
                } catch (HibernateException e1) {
                    System.out.println("Error rolling back transaction");
                }
// throw again the first exception
                throw e;
            }
        }
    }
}
