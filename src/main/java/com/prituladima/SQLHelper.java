package com.prituladima;

/**
 * Created by prituladima on 25.10.16.
 */
public class SQLHelper {


    //    DROP DATABASE IF EXISTS FILE_STATISTIC;
//    CREATE DATABASE FILE_STATISTIC;
//    USE FILE_STATISTIC;
    public static final String DATABASE_NAME = "FILE_STATISTIC";


    //    CREATE TABLE `FILES` (
//            `FileId` int(11) NOT NULL AUTO_INCREMENT,
//  `Name` varchar(255) DEFAULT NULL,
//  `Longest_word` varchar(255) DEFAULT NULL,
//  `Shortest_word` varchar(255) DEFAULT NULL,
//  `Number_of_lines` smallint DEFAULT 0,
//            `Average` smallint DEFAULT 0,
//    PRIMARY KEY (`FileId`)
//) ENGINE=InnoDB DEFAULT CHARSET=utf16;
    public static final String TABLE_FILES_NAME = "`FILES`";

    public static final String ID_FIELD_FILES = "`FileId`";
    public static final String NAME_FIELD_FILES = "`Name`";
    public static final String LONGEST_WORD_FIELD_FILES = "`Longest_word`";
    public static final String SHORTEST_WORD_FIELD_FILES = "`Shortest_word`";
    public static final String NUMBER_OF_LINES_FIELD_FILES = "`Number_of_lines`";
    public static final String AVERAGE_FIELD_FILES = "`Average`";


    //    CREATE TABLE `LINES` (
//            `Id` int(11) NOT NULL AUTO_INCREMENT,
//  `Line` text DEFAULT NULL,
//            `Longest_word` varchar(255) DEFAULT NULL,
//  `Shortest_word` varchar(255) DEFAULT NULL,
//  `Length` smallint DEFAULT 0,
//            `Average` smallint DEFAULT 0,
//            `FileId` int(11) NOT NULL DEFAULT '0',
//    PRIMARY KEY (`Id`),
//    KEY `FileId` (`FileId`),
//    CONSTRAINT `Line_ibfk_1`
//    FOREIGN KEY (`FileId`)
//    REFERENCES FILES (`FileId`)
//    ON DELETE CASCADE ON UPDATE CASCADE
//) ENGINE=InnoDB DEFAULT CHARSET=utf16;
    public static final String TABLE_LINES_NAME = "`LINES`";

    public static final String ID_FIELD_LINES = "`Id`";
    public static final String LINE_FIELD_LINES = "`Line`";
    public static final String LONGEST_WORD_FIELD_LINES = "`Longest_word`";
    public static final String SHORTEST_WORD_FIELD_LINES = "`Shortest_word`";
    public static final String LENGTH_FIELD_LINES = "`Length`";
    public static final String AVERAGE_FIELD_LINES = "`Average`";
    public static final String FILE_ID_FIELD_LINES = "`FileId`";


    public static final String COMMAND_INSERT = "INSERT INTO ";

    public static final String PREPARED_VALUES = "VALUES";


}
