package com.prituladima;

import java.io.File;
import java.util.List;

/**
 * Created by prituladima on 10/28/16.
 */
public class FolderHelper {

    public void listFilesForFolder(final File folder, List<File> fileList) {
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                listFilesForFolder(fileEntry, fileList);
            } else {
                fileList.add(fileEntry);
                System.out.println(fileEntry.getAbsolutePath());
            }
        }
    }

}
