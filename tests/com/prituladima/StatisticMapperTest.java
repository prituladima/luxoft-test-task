package com.prituladima;

import com.prituladima.entity.Statistic;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by prituladima on 10/22/16.
 */
public class StatisticMapperTest extends Assert {

    final String DARK_KNIGHT_LINE = "The Dark Knight is a 2008 superhero thriller film directed, produced, and co-written by Christopher Nolan.";
    final Integer LENGTH = 106;
    final String LONGER_WORD = "Christopher";
    final String SHORTEST_WORD = "a";
    final Integer AVERAGE = 5;

    private Statistic statisticToTest;

    @Before
    public void makeCalculation() {
        statisticToTest = Mapper.map(DARK_KNIGHT_LINE);
    }

    @Test
    public void testMapperIdentical() {
        assertEquals(DARK_KNIGHT_LINE, statisticToTest.getLine());
    }

    @Test
    public void testMapperLength() {
        assertEquals(LENGTH, statisticToTest.getLength());
    }

    @Test
    public void testMapperLongestWord() {
        assertEquals(LONGER_WORD, statisticToTest.getLongestWord());
    }

    @Test
    public void testMapperShortestWord() {
        assertEquals(SHORTEST_WORD, statisticToTest.getShortestWord());
    }

    @Test
    public void testMapperAverage() {
        assertEquals(AVERAGE, statisticToTest.getAverage());
    }

}
