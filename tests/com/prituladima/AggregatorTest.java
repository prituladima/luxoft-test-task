package com.prituladima;

import com.prituladima.entity.StatisticForFile;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.sql.Connection;
import java.util.List;

/**
 * Created by prituladima on 25.10.16.
 */
public class AggregatorTest extends Assert {


    @Test
    public void testFile(){
        File file = new File(FileReader.FILE_NAME);
        List<String> linesFromFile = new FileReader().readFromFile();
        assertNotNull(linesFromFile);

        StatisticForFile statisticForFile = Mapper.mapToStatisticForFile(linesFromFile, file);
        assertNotNull(statisticForFile);


        Connection connection = DatabaseHelper.initDataBase();
        assertNotNull(connection);

    }
}
